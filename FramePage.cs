﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

using Filminstitutet.Web.Models.Pages;

namespace NewsMachine.Addon
{
    /// <summary>
    /// Sida för generella texter på webbplatsen.
    /// </summary>
    [ContentType(GUID = "A99A71CE-95CD-4A0A-8280-5E88BF16511C",
                 DisplayName = "External Frame Page",
                 Description = "Renders a fullwidth iframe")]
    public class FramePage : SitePageData
    {
        [BackingType(typeof(PropertyString))]
        public virtual string Url { get; set; }

        [BackingType(typeof(PropertyNumber))]
        public virtual int Height { get; set; }
    }
}
