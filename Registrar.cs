﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using EPiServer.Web.Mvc;
using EPiServer.Shell;

namespace NewsMachine.Addon
{
    [ServiceConfiguration(typeof(IViewTemplateModelRegistrator))]
    public class Registrar : IViewTemplateModelRegistrator
    {
        /// <summary>
        /// Registers renderers/templates which are not automatically discovered, 
        /// i.e. partial views whose names does not match a content type's name.
        /// </summary>
        /// <remarks>
        /// Using only partial views instead of controllers for blocks and page partials
        /// has performance benefits as they will only require calls to RenderPartial instead of
        /// RenderAction for controllers.
        /// Registering partial views as templates this way also enables specifying tags and 
        /// that a template supports all types inheriting from the content type/model type.
        /// </remarks>
        public void Register(TemplateModelCollection viewTemplateModelRegistrator)
        {
            //var p = "~/modules/NewsMachine.Addon/IFrameBlock.ascx"; //Paths.ToResource(typeof(IFrameBlock), "IFrameBlock.cshtml");
            var p = Paths.ToResource(typeof(FramePage), "FramePage.cshtml");
            //var p = Paths.ToResource(typeof(IFrameBlockType), "IFrameBlock.ascx");

            viewTemplateModelRegistrator.Add(typeof(FramePage), new TemplateModel
            {
                AvailableWithoutTag = true,
                Path = p
            });
        }
    }
}
